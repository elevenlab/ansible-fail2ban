Ansible Role: Fail2ban
===================

Install and configure [Fail2ban](https://github.com/fail2ban/fail2ban) on Debian servers. 

At this time it only supports `ssh`, `ssh-ddos` and `recidive` jails

----------

Role variables
-------------

Some of available variables are listed below, along with default values (see `defaults/main.yml`):

	fail2ban_logtarget: /var/log/fail2ban.log
The log file where fail2ban will log the `ban`/`unban` and any debug information.

	fail2ban_loglevel: 3
	
The log level, available numbers respect the fail2ban conf file: `error=1`, `warning=2`, `info=3`, `debug=4`.

	fail2ban_backend: polling
The method with fail2ban check changes to log files. By default *should* be set to `auto` but Debian 8 has a bug with `pynotify` so `polling` is used instead as default.

	fail2ban_ignoreip: '127.0.0.1/8'
	fail2ban_bantime: 600
	fail2ban_findtime: 600
	fail2ban_maxretry: 3
	fail2ban_usedns: warn
	fail2ban_destmail: root@localhost
	fail2ban_sendermail: fail2ban@localhost
	fail2ban_banaction: iptables-multiport
	fail2ban_mta: sendmail
	fail2ban_protocol: tcp
	fail2ban_chain: INPUT
Global variables used to set parameters common to all the jails.
	
	fail2ban_ssh_enabled: 'true'
	fail2ban_ssh_*
`ssh` jail specific parameter, once enabled all other variable will be available in the `jail.conf`
	
	fail2ban_ssh_ddos_enabled: 'true'
	fail2ban_ssh_ddos_*
`ssh-ddos` jail specific parameter, once enabled all other variable will be available in the `jail.conf`

	fail2ban_recidive_enabled: 'true'
	fail2ban_recidive_*
`recidive` jail specific parameter. If it is enabled the variable `fail2ban_loglevel` cannot be set above `3` 
	
>"which might then cause fail2ban to fall into  an infinite loop constantly feeding" itself with non-informative lines"

The role provide this check returning error if this requirement is not setisfied. 
`recidive` will provide a ban for ip wich continues to fails the rules of `fail2ban`

Dependencies
-------------------
None.

Example Playbook
--------------------------
	- name: configure fail2ban
	  hosts: manager
	  vars:
		  fail2ban_recidive_action: "iptables-allports[name=recidive] sendmail-whois-lines[name=recidive, logpath=/var/log/fail2ban.log]"

	  roles:
	    -  fail2ban


TODO
-------------
* add more jails